#!/bin/sh
# script mounts my NAS

SERVPATH="//192.168.1.45/Volume_1/"
CREDPATH=~/.NAS-Tobias_cred
MOUNTPATH=~/mnt/nas
EXPLORERPATH=/

printhelp () {
  echo "Usage: $0           mounts NAS"
  echo "       $0 -h        show this message"
  echo "       $0 -e        start file explorer after mount"
  echo "       $0 -u        unmount NAS"
}

mountNAS () {
  if checkmount ; then
    echo "Allready mounted!"
    return 1
  else
    sudo mount -t cifs -o credentials=$CREDPATH $SERVPATH  $MOUNTPATH
    return 0
  fi
}

explorer () {
  if ! checkmount ; then
    mountNAS
  fi
  nautilus ${MOUNTPATH}${EXPLORERPATH} &> /dev/null &
}

checkmount () {
  if $(mount | grep $SERVPATH &> /dev/null) ; then
    return 0
  else
    return 1
  fi
}

umountNAS () {
  if checkmount ; then
    sudo umount -R $MOUNTPATH
    return 0
  else
    echo "NAS not mounted!"
    return 1
  fi
}

## Main

case $1 in
  "") echo "Mounting on ${MOUNTPATH}" ; mountNAS ; exit 0 ;;
  -h|--help) printhelp ; exit 0 ;;
  -e|--explorer) explorer ;;
  -u|--umount) umountNAS ; exit 0;;
  *)  ;;
esac

