#!/bin/sh
# script mounts the HSR DFS

SERVPATH="//hsr.ch/root/alg/skripte/"
CREDPATH=~/.hsr.ch_cred
MOUNTPATH=~/mnt/hsrdfs
EXPLORERPATH=/Informatik/Fachbereich

printhelp () {
  echo "Usage: $0           do mount hsr-dfs"
  echo "       $0 -h        show this message"
  echo "       $0 -e        start file explorer after mount"
  echo "       $0 -u        unmount hsr-dfs"
}

mountDFS () {
  if checkmount ; then
    echo "Allready mounted!"
    return 1
  else
    sudo mount -t cifs -o credentials=$CREDPATH $SERVPATH  $MOUNTPATH
    return 0
  fi
}

explorer () {
  if ! checkmount ; then
    mountDFS
  fi
  nautilus ${MOUNTPATH}${EXPLORERPATH} &> /dev/null &
}

checkmount () {
  if $(mount | grep $SERVPATH &> /dev/null) ; then
    return 0
  else
    return 1
  fi
}

umountDFS () {
  if checkmount ; then
    sudo umount -R $MOUNTPATH
    return 0
  else
    echo "HSR-DFS not mounted!"
    return 1
  fi
}

## Main

case $1 in
  "") echo "Mounting on ${MOUNTPATH}" ; mountDFS ; exit 0 ;;
  -h|--help) printhelp ; exit 0 ;;
  -e|--explorer) explorer ;;
  -u|--umount) umountDFS ; exit 0;;
  *)  ;;
esac

